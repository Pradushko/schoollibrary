package com.epam.test.controller;

import com.epam.test.model.Literature;
import com.epam.test.model.LiteratureType;
import com.epam.test.model.Model;
import com.epam.test.model.Pupil;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class ControllerTest {
    @Test
    public void getAvaibleLiterature() throws Exception {
        Pupil pupil = new Pupil("kolya", LocalDate.now());
        Literature literature1 = new Literature("Война и мир", LiteratureType.Book);
        Literature literature2 = new Literature("Война и мир2", LiteratureType.Book);
        Literature literature3 = new Literature("Война и мир3", LiteratureType.Book);
        Model model = new Model();
        /**
         * В библиотеке 3 книги и 1 пользователь
         */
        model.getAllLiteratures().add(literature1);
        model.getAllLiteratures().add(literature2);
        model.getAllLiteratures().add(literature3);
        model.getPupils().add(pupil);
        Controller controller = new Controller(model);
        /**
         * 1 книгу взял ученик
         */
        literature1.setCurrentOwner(pupil);
        /**
         *  Доступно 2 книги
         */

        assertEquals(2, controller.getAvailableLiterature().size());
    }

    @Test
    public void getOftenReadingPupils() throws Exception {
        Pupil pupil1 = new Pupil("Sasha", LocalDate.of(2011, 10, 14));
        Pupil pupil2 = new Pupil("Katya", LocalDate.of(2012, 11, 18));
        Pupil pupil3 = new Pupil("Anya", LocalDate.of(2012, 11, 18));
        Pupil pupil4 = new Pupil("Vika", LocalDate.of(2012, 11, 18));
        Pupil pupil5 = new Pupil("Tolya", LocalDate.of(2013, 12, 21));
        Literature literature1 = new Literature("Война и мир", LiteratureType.Book);
        Literature literature2 = new Literature("Война и мир2", LiteratureType.Book);
        Literature literature3 = new Literature("Война и мир3", LiteratureType.Book);
        Literature literature4 = new Literature("Война и мир4", LiteratureType.Book);
        /**
         * В библиотеке 4 книги и 5 пользователей
         */
        Model model = new Model();
        model.getAllLiteratures().add(literature1);
        model.getAllLiteratures().add(literature2);
        model.getAllLiteratures().add(literature3);
        model.getAllLiteratures().add(literature4);
        model.getPupils().add(pupil1);
        model.getPupils().add(pupil2);
        model.getPupils().add(pupil3);
        model.getPupils().add(pupil4);
        model.getPupils().add(pupil5);
        Controller controller = new Controller(model);
        /**
         * 1-й ученик прочитал 3 книги
         * 2-й ученик прочитал 2 книги
         * 3-й ученик прочитал 4 книги
         * 4-й ученик прочитал 1 книгу
         * 5-й ученик прочитал 1 книгу
         */
        pupil1.getReadLiterature().add(literature1);
        pupil1.getReadLiterature().add(literature2);
        pupil1.getReadLiterature().add(literature3);
        pupil2.getReadLiterature().add(literature1);
        pupil2.getReadLiterature().add(literature2);
        pupil3.getReadLiterature().add(literature1);
        pupil3.getReadLiterature().add(literature2);
        pupil3.getReadLiterature().add(literature3);
        pupil3.getReadLiterature().add(literature4);
        pupil4.getReadLiterature().add(literature2);
        pupil5.getReadLiterature().add(literature1);
        /**
         * 3 ученика прочитали 2 и более книг
         */
        assertEquals(3, controller.getOftenReadingPupils().size());

        /**
         * В списке ученики располагаются по возратанию количества прочитанных книг
         * Katya - 2
         * Sasha - 3
         * Anya - 4
         */
        assertEquals("Katya", controller.getOftenReadingPupils().get(0).getName());
        assertEquals("Sasha", controller.getOftenReadingPupils().get(1).getName());
        assertEquals("Anya", controller.getOftenReadingPupils().get(2).getName());
    }

    @Test
    public void getSeldomReadingPupils() throws Exception {
        Pupil pupil1 = new Pupil("Sasha", LocalDate.of(2011, 10, 14));
        Pupil pupil2 = new Pupil("Katya", LocalDate.of(2012, 11, 18));
        Pupil pupil3 = new Pupil("Anya", LocalDate.of(2012, 11, 18));
        Pupil pupil4 = new Pupil("Vika", LocalDate.of(2012, 11, 18));
        Pupil pupil5 = new Pupil("Tolya", LocalDate.of(2013, 12, 21));
        Pupil pupil6 = new Pupil("Dima", LocalDate.of(2010, 9, 19));
        Literature literature1 = new Literature("Война и мир", LiteratureType.Book);
        Literature literature2 = new Literature("Война и мир2", LiteratureType.Book);
        Literature literature3 = new Literature("Война и мир3", LiteratureType.Book);
        Literature literature4 = new Literature("Война и мир4", LiteratureType.Book);

        /**
         * В библиотеке 4 книги и 6 учеников
         */
        Model model = new Model();
        model.getAllLiteratures().add(literature1);
        model.getAllLiteratures().add(literature2);
        model.getAllLiteratures().add(literature3);
        model.getAllLiteratures().add(literature4);
        model.getPupils().add(pupil1);
        model.getPupils().add(pupil2);
        model.getPupils().add(pupil3);
        model.getPupils().add(pupil4);
        model.getPupils().add(pupil5);
        model.getPupils().add(pupil6);
        Controller controller = new Controller(model);

        pupil1.getReadLiterature().add(literature4);
        pupil2.getReadLiterature().add(literature1);
        pupil3.getReadLiterature().add(literature2);
        pupil3.getReadLiterature().add(literature3);
        pupil5.getReadLiterature().add(literature3);
        pupil5.getReadLiterature().add(literature2);
        pupil6.getReadLiterature().add(literature2);
        pupil6.getReadLiterature().add(literature1);
        pupil6.getReadLiterature().add(literature3);

        /**
         *  6-й ученик прочитал более 2-х книг, поэтому в список не попадает
         */
        assertEquals(5, controller.getSeldomReadingPupils().size());
        /**
         *  Sasha самый старший, из попавших список
         */
        assertEquals("Sasha", controller.getSeldomReadingPupils().get(0).getName());
        /**
         * Anya,Katya,Vika одного возраста и младше Sasha
         * Anya - 2 книги, Katya - 1, Vika - 0
         */
        assertEquals("Anya", controller.getSeldomReadingPupils().get(1).getName());
        assertEquals("Katya", controller.getSeldomReadingPupils().get(2).getName());
        assertEquals("Vika", controller.getSeldomReadingPupils().get(3).getName());
        /**
         * Tolya самый младший
         */
        assertEquals("Tolya", controller.getSeldomReadingPupils().get(4).getName());
    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void setCurrentReader() throws Exception {
        Pupil pupil1 = new Pupil("Sasha", LocalDate.of(2011, 10, 14));
        Pupil pupil2 = new Pupil("Vasya", LocalDate.of(2010, 9, 12));
        Literature literature1 = new Literature("Война и мир", LiteratureType.Book);
        Model model = new Model();
        model.getPupils().add(pupil1);
        model.getPupils().add(pupil2);
        model.getAllLiteratures().add(literature1);
        Controller controller = new Controller(model);

        /**
         *  Книга находится в библиотеке ( не имеет ученика-владельца )
         */
        assertNull(literature1.getCurrentOwner());

        /**
         *  Поэтому ее можно взять
         */
        controller.setCurrentReader(pupil1, literature1);
        assertEquals(pupil1, literature1.getCurrentOwner());

        /**
         * Попытка взять ее в библиотеке другим учеником приведет к IllegalArgumentException
         */
        controller.setCurrentReader(pupil2, literature1);
    }

    @Test
    public void returnLiteratureToLibrary() throws Exception {
        Pupil pupil1 = new Pupil("Sasha", LocalDate.of(2011, 10, 14));
        Literature literature1 = new Literature("Война и мир", LiteratureType.Book);
        Literature literature2 = new Literature("Война и мир2", LiteratureType.Book);
        Literature literature3 = new Literature("Аргументы и факты", LiteratureType.Newspaper);

        Model model = new Model();
        model.getPupils().add(pupil1);
        model.getAllLiteratures().add(literature1);
        model.getAllLiteratures().add(literature2);
        model.getAllLiteratures().add(literature3);
        Controller controller = new Controller(model);
        /**
         * Изначально прочитанных книг 0
         */
        assertEquals(0, pupil1.getReadLiterature().size());
        /**
         * Книги не добавляются в "прочитанные" после получения из библиотеки
         */
        literature1.setCurrentOwner(pupil1);
        literature2.setCurrentOwner(pupil1);
        literature3.setCurrentOwner(pupil1);
        assertEquals(0, pupil1.getReadLiterature().size());
        /**
         * В "прочитанное" литература добавляется только после сдачи книги в библиотеку и при условии, что тип литературы - книга
         */
        controller.returnLiteratureToLibrary(pupil1, literature1);
        controller.returnLiteratureToLibrary(pupil1, literature2);
        controller.returnLiteratureToLibrary(pupil1, literature3);
        assertEquals(2, pupil1.getReadLiterature().size());
    }

}