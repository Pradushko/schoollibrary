package com.epam.test;

import com.epam.test.controller.Controller;
import com.epam.test.model.Literature;
import com.epam.test.model.Pupil;
import com.epam.test.model.LiteratureType;
import com.epam.test.model.Model;
import com.epam.test.view.View;

import java.time.LocalDate;


public class Main {
    public static void main(String[] args) {
        /**
         *  add 10 pupils
         */
        Pupil vasya = new Pupil("Vasya", LocalDate.of(2015, 12, 17));
        Pupil kolya = new Pupil("Kolya", LocalDate.of(2014, 10, 19));
        Pupil vitya = new Pupil("Vitya", LocalDate.of(2013, 9, 15));
        Pupil masha = new Pupil("Masha", LocalDate.of(2012, 11, 18));
        Pupil pasha = new Pupil("Pasha", LocalDate.of(2012, 11, 18));
        Pupil dasha = new Pupil("Dasha", LocalDate.of(2012, 11, 18));
        Pupil sasha = new Pupil("Sasha", LocalDate.of(2012, 11, 17));
        Pupil galya = new Pupil("Galya", LocalDate.of(2012, 11, 18));
        Pupil valya = new Pupil("Valya", LocalDate.of(2012, 11, 18));
        Pupil pavel = new Pupil("Pavel", LocalDate.of(2013, 10, 18));
        /**
         * add literature
         */
        Literature literature1 = new Literature("Война и мир", LiteratureType.Book);
        Literature literature2 = new Literature("Война и мир2", LiteratureType.Book);
        Literature literature3 = new Literature("Война и мир3", LiteratureType.Book);
        Literature literature4 = new Literature("Война и мир4", LiteratureType.Book);
        Literature literature5 = new Literature("Война и мир5", LiteratureType.Book);
        Literature literature6 = new Literature("Война и мир6", LiteratureType.Book);
        Literature literature7 = new Literature("Война и мир7", LiteratureType.Book);
        Literature literature8 = new Literature("Война и мир8", LiteratureType.Book);
        Literature literature9 = new Literature("Война и мир9", LiteratureType.Book);

        /**
         * Create a model
         */
        Model model = new Model();
        model.getAllLiteratures().add(literature1);
        model.getAllLiteratures().add(literature2);
        model.getAllLiteratures().add(literature3);
        model.getAllLiteratures().add(literature4);
        model.getAllLiteratures().add(literature5);
        model.getAllLiteratures().add(literature6);
        model.getAllLiteratures().add(literature7);
        model.getAllLiteratures().add(literature8);
        model.getAllLiteratures().add(literature9);
        model.getPupils().add(vasya);
        model.getPupils().add(kolya);
        model.getPupils().add(vitya);
        model.getPupils().add(masha);
        model.getPupils().add(pasha);
        model.getPupils().add(dasha);
        model.getPupils().add(sasha);
        model.getPupils().add(galya);
        model.getPupils().add(valya);
        model.getPupils().add(pavel);

        /**
         * Create controller
         */
        Controller controller = new Controller(model);

        /**Create view
         *
         */
        View view = new View(controller);

        controller.setCurrentReader(vasya, literature1);
        controller.returnLiteratureToLibrary(vasya, literature1);
        controller.setCurrentReader(vasya, literature2);
        controller.returnLiteratureToLibrary(vasya, literature2);
        controller.setCurrentReader(kolya, literature1);
        controller.returnLiteratureToLibrary(kolya, literature1);
        controller.setCurrentReader(masha, literature1);
        controller.returnLiteratureToLibrary(masha, literature1);
        controller.setCurrentReader(masha, literature2);
        controller.returnLiteratureToLibrary(masha, literature2);
        controller.setCurrentReader(masha, literature3);
        controller.returnLiteratureToLibrary(masha, literature3);
        controller.setCurrentReader(pasha, literature1);
        controller.setCurrentReader(pasha, literature2);
        controller.returnLiteratureToLibrary(pasha, literature1);
        controller.returnLiteratureToLibrary(pasha, literature2);
        controller.setCurrentReader(dasha, literature1);
        controller.setCurrentReader(galya, literature2);
        controller.returnLiteratureToLibrary(dasha, literature1);
        controller.returnLiteratureToLibrary(galya, literature2);
        controller.setCurrentReader(pavel, literature2);
        controller.returnLiteratureToLibrary(pavel, literature2);
        controller.setCurrentReader(pavel, literature2);

        /** Print the first report
         *
         */
        view.printAvailableLiterature();
        /**
         * Print the second report
         */
        view.printMuchReadingReport();
        /**
         * Print  the third report
         */
        view.printLessReadingReport();
    }
}
