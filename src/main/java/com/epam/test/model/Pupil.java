package com.epam.test.model;

import com.epam.test.model.Literature;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Pupil {
    /**
     * The name of the pupil
     */
    private final String name;
    /**
     * The birthday of the pupil
     */
    private final LocalDate birthdate;
    /**
     * The Set stores books, that the pupil has read
     */
    private final Set<Literature> readLiterature = new HashSet<>();

    public Pupil(String name, LocalDate birthdate) {
        this.name = name;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }


    public Set<Literature> getReadLiterature() {
        return readLiterature;
    }

    @Override
    public String toString() {
        return "Pupil{" +
                "name='" + name + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
