package com.epam.test.model;

/**
 * The type of the subject of reading
 */
public enum LiteratureType {
    Book, Article, Journal, Newspaper
}
