package com.epam.test.model;

public class Literature {
    /**
     * The title of the literature
     */
    private final String title;
    /**
     * The type of the literature
     * It's included "book", "article", "journal", "newspaper"
     */
    private final LiteratureType literatureType;
    /**
     * The current Literature owner
     * If there isn't owner of the literature means the book is in the library
     */
    private Pupil currentOwner;

    public Literature(String title, LiteratureType literatureType) {
        this.title = title;
        this.literatureType = literatureType;
    }

    public String getTitle() {
        return title;
    }

    public LiteratureType getLiteratureType() {
        return literatureType;
    }

    public Pupil getCurrentOwner() {
        return currentOwner;
    }

    public void setCurrentOwner(Pupil currentOwner) {
        this.currentOwner = currentOwner;
    }

    @Override
    public String toString() {
        return "title '" + title + '\'' + ", " + literatureType;
    }
}

