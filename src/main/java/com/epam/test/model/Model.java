package com.epam.test.model;

import java.util.ArrayList;
import java.util.List;

/**
 * The class stores all data
 */
public class Model {
    /**
     * the list contains all literature
     */
    private List<Literature> allLiteratures = new ArrayList<>();
    /**
     * the list contains all pupils
     */
    private List<Pupil> pupils = new ArrayList<>();

    public List<Literature> getAllLiteratures() {
        return allLiteratures;
    }

    public void setAllLiteratures(List<Literature> allLiteratures) {
        this.allLiteratures = allLiteratures;
    }

    public List<Pupil> getPupils() {
        return pupils;
    }

    public void setPupils(List<Pupil> pupils) {
        this.pupils = pupils;
    }
}
