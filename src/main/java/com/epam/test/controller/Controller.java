package com.epam.test.controller;

import com.epam.test.model.Literature;
import com.epam.test.model.LiteratureType;
import com.epam.test.model.Pupil;
import com.epam.test.model.Model;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class manages the data (Model)
 */
public class Controller {
    private final Model model;

    public Controller(Model model) {
        this.model = model;
    }

    /**
     * @return the list of available for reading books, articles, journals and newspapers
     * <p>
     * The returned list includes only those literatures that don't have a current owner
     */
    public List<Literature> getAvailableLiterature() {
        return model.getAllLiteratures().stream()
                .filter(l -> Objects.isNull(l.getCurrentOwner()))
                .collect(Collectors.toList());
    }

    /**
     * @return the sorted list
     * <p>
     * The data in the list sorted out pupils by the number of books read upwards
     * The final list includes pupils, who have read 2 or more books
     */
    public List<Pupil> getOftenReadingPupils() {
        List<Pupil> allPupils = model.getPupils();
        List<Pupil> manyReadPupils = new ArrayList<>();
        for (Pupil pupil : allPupils) {
            if (pupil.getReadLiterature().size() >= 2) {
                manyReadPupils.add(pupil);
            }
        }
        Collections.sort(manyReadPupils, new Comparator<Pupil>() {
            @Override
            public int compare(Pupil o1, Pupil o2) {
                int countBooks1 = o1.getReadLiterature().size();
                int countBooks2 = o2.getReadLiterature().size();
                return countBooks1 - countBooks2;
            }
        });
        return manyReadPupils;
    }

    /**
     * @return the sorted list
     * <p>
     * the list sorted out by the pupils date of birth and the number of books read
     * (from the oldest to the youngest pupils, and from the largest to the
     * smallest number of books read, i.e. downward).
     * The final list includes pupils, who have read 2 or less books
     */
    public List<Pupil> getSeldomReadingPupils() {
        List<Pupil> allPupils = model.getPupils();
        List<Pupil> lessReadPupils = new ArrayList<>();
        for (Pupil pupil : allPupils) {
            if (pupil.getReadLiterature().size() <= 2) {
                lessReadPupils.add(pupil);
            }
        }
        Collections.sort(lessReadPupils, new Comparator<Pupil>() {
            @Override
            public int compare(Pupil o1, Pupil o2) {
                LocalDate date1 = o1.getBirthdate();
                LocalDate date2 = o2.getBirthdate();
                if (date1.isAfter(date2)) {
                    return 1;
                }
                if (date1.isBefore(date2)) {
                    return -1;
                }
                if (date1.isEqual(date2)) {
                    return o2.getReadLiterature().size() - o1.getReadLiterature().size();
                }
                return 1;
            }
        });
        return lessReadPupils;
    }

    /**
     * @param pupil is the next owner of the books
     * @param literature  that the student is trying to take
     *              <p>
     *              The pupil will be able to take the literature, if it's in the library
     */
    public void setCurrentReader(Pupil pupil, Literature literature) {
        if (literature.getCurrentOwner() == null) {
            literature.setCurrentOwner(pupil);
        } else if(!literature.getCurrentOwner().equals(pupil)){
            throw new IllegalArgumentException("Current literature is owned by other pupil");
        }
    }

    /**
     * @param pupil
     * @param literature  The pupil is returning the literature.
     *              If the type of literature is book then the literature added to Set<Literature> readBook
     *              The library is new owner literature
     */
    public void returnLiteratureToLibrary(Pupil pupil, Literature literature) {
        if (literature.getLiteratureType()== LiteratureType.Book){
            pupil.getReadLiterature().add(literature);
        }
        literature.setCurrentOwner(null);
    }
}


