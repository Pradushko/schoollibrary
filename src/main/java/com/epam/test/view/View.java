package com.epam.test.view;

import com.epam.test.controller.Controller;
import com.epam.test.model.Literature;
import com.epam.test.model.Pupil;

import java.time.LocalDate;
import java.util.List;

/**
 * This class receives data (Model) from the controller and displays it on the screen
 */
public class View {
    private Controller controller;

    public View(Controller controller) {
        this.controller = controller;
    }

    /**
     * This method displays all the literature that are in the library
     * If the book was taken by a pupil, then it's not in this list
     */
    public void printAvailableLiterature() {
        System.out.println("**********Task 1***********");
        List<Literature> availableLiterature = controller.getAvailableLiterature();
        for (Literature literature : availableLiterature) {
            System.out.println(literature);
        }
    }

    /**
     * This method displays on the screen pupils name sorted by the increase in the number of books read
     * The screen displays only those names of students, who read 2 or more books (pre-sorted in the controller)
     */
    public void printMuchReadingReport() {
        System.out.println("***********Task 2***********");
        List<Pupil> bestReader = controller.getOftenReadingPupils();
        for (Pupil pupil : bestReader) {
            String pupilName = pupil.getName();
            int countReadBooks = pupil.getReadLiterature().size();
            System.out.println(pupilName + ":" + countReadBooks);
        }
    }

    /**
     * This method displays on the screen pupils name sorted out by the pupils date of birth and the number of books read
     * (from the oldest to the youngest pupils, and from the largest to the
     * smallest number of books read, i.e. downward).
     * The screen displays only those names of students, who have read 2 or less books (pre-sorted in the controller)
     */

    public void printLessReadingReport() {
        System.out.println("************Task 3*************");
        List<Pupil> lessReader = controller.getSeldomReadingPupils();
        for (Pupil pupil : lessReader) {
            String pupilName = pupil.getName();
            LocalDate birthday = pupil.getBirthdate();
            int countReadBooks = pupil.getReadLiterature().size();
            System.out.println(pupilName + ", " + birthday + ": " + countReadBooks);
        }
    }
}
